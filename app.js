// Simple node express server with three GET endpoints

const express = require('express');
const path = require('path');
const data = require('./data.json');

const app = express();

const { PORT = 8080 } = process.env;

app.use('/assets', express.static(path.join(__dirname, 'public', 'static')));
app.use(express.json());
app.use(express.urlencoded( { extended: true } ));

app.get('/', (req,res) => {
    
   return res.sendFile(path.join(__dirname, 'public', 'index.html'));
})
app.get('/restaurants', (req,res) => {
   
   return res.sendFile(path.join(__dirname, 'public', 'restaurants.html'));
})

app.get('/data', (req,res) => {
   res.send(data);
   res.end();
 })

app.listen(PORT, () => console.log(`Server started on port:  ${PORT}`))