
let dataArray = [];
let reviewIDs = [];

// Fetch the data from data.json. Gets the container element and then loops thorugh all restaurant objects and displays them
fetch('/data')
    .then(resp => resp.json())
    .then(data => {
        const container = document.getElementById('container');
        dataArray = data;
        for (let i = 0; i < data.length; i++) {
            
            container.innerHTML += `
            <div class="jumbotron">
            <h1 class="display-4">${data[i].name}</h1>
            <p class="lead">${data[i].description}</p>
            <hr class="my-4">
            <h3> Average rating: ${data[i].rating} / 5</h3>
             <img  src="assets/${data[i].image}" alt=""> 

             <h4 class="reviewTag" id="reviewTag${i}" onclick="toggleReviews(${i})">Click to show reviews <i class="fa fa-caret-down" aria-hidden="true"></i></h4>
            <div id="reviewContainer${i}" >
            </div>
          </div>
          <br>
            `
        }

    })

    // Toggle for the reviews takes id as argument to know which reviews to display/hide
let toggleReviews = (id) => {
    
    const reviewTagDown = document.querySelector('.fa-caret-down');
    const reviewTagUp = document.querySelector('.fa-caret-up');
    const reviewContainer = document.getElementById('reviewContainer'+id);
    
    for (let index = 0; index < reviewIDs.length; index++) {
        if (reviewIDs[index] == id) {
            let removeID = reviewIDs.indexOf(id);
            reviewIDs.splice(removeID, 1);
            reviewContainer.innerHTML = "";
            reviewTagUp.setAttribute("class", "fa fa-caret-down")
            return;
        }
    }
    reviewTagDown.setAttribute("class", "fa fa-caret-up")
    reviewIDs.push(id)
 
 for (let j = 0; j < dataArray[id].reviews.length; j++) {
        reviewContainer.innerHTML += `
        <p>BY: ${dataArray[id].reviews[j].author}</p>
        <p>${dataArray[id].reviews[j].review}</p>
        <p>Rating: ${dataArray[id].reviews[j].rating}</p>
        ------------------------------------------------------
        <br>
        `
 }
     
 }
